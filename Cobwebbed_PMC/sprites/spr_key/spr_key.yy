{
    "id": "dc4964fb-1a8f-4342-9440-af737d48173b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_key",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 51,
    "bbox_left": 8,
    "bbox_right": 59,
    "bbox_top": 20,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7d46a1ed-7f33-4f2a-8747-dd8394c2ffa8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dc4964fb-1a8f-4342-9440-af737d48173b",
            "compositeImage": {
                "id": "335fdadf-9cd1-4c23-8890-d6f4ad6cf270",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7d46a1ed-7f33-4f2a-8747-dd8394c2ffa8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fbccef3b-76dc-4b28-9fab-36bfb031ccf8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7d46a1ed-7f33-4f2a-8747-dd8394c2ffa8",
                    "LayerId": "1551e73e-a8d0-4508-9887-d6385c61534b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "1551e73e-a8d0-4508-9887-d6385c61534b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "dc4964fb-1a8f-4342-9440-af737d48173b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}