{
    "id": "5664eb16-e572-4ebd-969f-d4e83099582f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_skull",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 51,
    "bbox_left": 4,
    "bbox_right": 51,
    "bbox_top": 12,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1290500a-7374-4ea0-a9fe-f41500e6596b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5664eb16-e572-4ebd-969f-d4e83099582f",
            "compositeImage": {
                "id": "cb5c69ad-db2f-4e95-8f50-91ea99a61aa8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1290500a-7374-4ea0-a9fe-f41500e6596b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c9a4e472-1b61-4cc5-9ea6-7b326a4124a6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1290500a-7374-4ea0-a9fe-f41500e6596b",
                    "LayerId": "6c4ade5c-d51c-4a96-8883-b1f54d42eb02"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "6c4ade5c-d51c-4a96-8883-b1f54d42eb02",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5664eb16-e572-4ebd-969f-d4e83099582f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}