{
    "id": "c0a9bddf-06fe-411f-a028-bb07d55f5887",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_slot_off",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0a537adb-b70d-4920-b8da-3bbd667e2ce5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c0a9bddf-06fe-411f-a028-bb07d55f5887",
            "compositeImage": {
                "id": "b6579acb-849f-4175-93e0-d8825de3bc77",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0a537adb-b70d-4920-b8da-3bbd667e2ce5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "200ae185-587a-4129-9707-ecceca6f9a14",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0a537adb-b70d-4920-b8da-3bbd667e2ce5",
                    "LayerId": "a6c57d19-25d3-42f3-875c-9379a7d94440"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "a6c57d19-25d3-42f3-875c-9379a7d94440",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c0a9bddf-06fe-411f-a028-bb07d55f5887",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}