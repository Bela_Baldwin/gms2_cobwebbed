{
    "id": "d2ce1c2a-bd5f-47a1-b916-258c8a1456de",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_book",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 55,
    "bbox_left": 4,
    "bbox_right": 59,
    "bbox_top": 8,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0a886ab3-d9ed-4cd7-b7a7-9b3596f7498f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d2ce1c2a-bd5f-47a1-b916-258c8a1456de",
            "compositeImage": {
                "id": "6a8302fe-9d75-4426-a7e8-819b2ca8b02f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0a886ab3-d9ed-4cd7-b7a7-9b3596f7498f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2dd86629-e42b-44b4-912c-b36212b03943",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0a886ab3-d9ed-4cd7-b7a7-9b3596f7498f",
                    "LayerId": "7ca1e4e4-bd39-4ff6-883e-cb88a0a4c54e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "7ca1e4e4-bd39-4ff6-883e-cb88a0a4c54e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d2ce1c2a-bd5f-47a1-b916-258c8a1456de",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}