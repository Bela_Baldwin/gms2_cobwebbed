{
    "id": "517ec9a9-2383-4740-9d1e-727a2eed99d6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_well",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 59,
    "bbox_left": 8,
    "bbox_right": 55,
    "bbox_top": 8,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8c270b1e-ddf5-4dbc-a56c-19b6940f198e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "517ec9a9-2383-4740-9d1e-727a2eed99d6",
            "compositeImage": {
                "id": "2fa82042-c474-4347-ae41-b63be24591c2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8c270b1e-ddf5-4dbc-a56c-19b6940f198e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c80dc35d-5a52-44af-afff-342bfb100c68",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8c270b1e-ddf5-4dbc-a56c-19b6940f198e",
                    "LayerId": "a3175aa6-c536-4602-917a-c080dc45b366"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "a3175aa6-c536-4602-917a-c080dc45b366",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "517ec9a9-2383-4740-9d1e-727a2eed99d6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}