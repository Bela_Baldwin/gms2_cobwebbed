{
    "id": "9dc373f7-2cd6-4cdb-af2b-eeda2f30f434",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_gold",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c83eabb9-9f05-47a3-83be-2b838071aacc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9dc373f7-2cd6-4cdb-af2b-eeda2f30f434",
            "compositeImage": {
                "id": "0fcb1170-80a7-476d-9269-38e294f5fed1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c83eabb9-9f05-47a3-83be-2b838071aacc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4179a23a-6082-4c8f-9a7b-ece76a9d013a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c83eabb9-9f05-47a3-83be-2b838071aacc",
                    "LayerId": "8e22ebc8-e71c-49d7-be0a-29ddfed4167b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "8e22ebc8-e71c-49d7-be0a-29ddfed4167b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9dc373f7-2cd6-4cdb-af2b-eeda2f30f434",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}