{
    "id": "6a2ad039-0066-44a5-815f-1171f3f88960",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_zombie_walk_left",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 55,
    "bbox_left": 12,
    "bbox_right": 55,
    "bbox_top": 12,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3b6cf358-1c27-4c3f-879e-2a01c03107e1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6a2ad039-0066-44a5-815f-1171f3f88960",
            "compositeImage": {
                "id": "0fdf089a-8a08-4a37-8e20-8288e7af9aae",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3b6cf358-1c27-4c3f-879e-2a01c03107e1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b38fd1fa-b56c-4443-97fc-584970a1d938",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3b6cf358-1c27-4c3f-879e-2a01c03107e1",
                    "LayerId": "aafc7fba-c164-4874-b9db-6fb02e70b4b0"
                }
            ]
        },
        {
            "id": "5481dc88-da90-4215-8783-18855afde2eb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6a2ad039-0066-44a5-815f-1171f3f88960",
            "compositeImage": {
                "id": "c98d769f-efc9-42cc-902d-1513618cbaed",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5481dc88-da90-4215-8783-18855afde2eb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "884be26c-57a6-4414-b0ca-c9880804b9ed",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5481dc88-da90-4215-8783-18855afde2eb",
                    "LayerId": "aafc7fba-c164-4874-b9db-6fb02e70b4b0"
                }
            ]
        },
        {
            "id": "7d29c1d6-873d-449e-873c-3d3276827d9d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6a2ad039-0066-44a5-815f-1171f3f88960",
            "compositeImage": {
                "id": "fcc9fcb6-c2d8-45f0-955d-17a71553d3d8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7d29c1d6-873d-449e-873c-3d3276827d9d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "982ee014-3b1a-450d-bd40-0ddd4d04e303",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7d29c1d6-873d-449e-873c-3d3276827d9d",
                    "LayerId": "aafc7fba-c164-4874-b9db-6fb02e70b4b0"
                }
            ]
        },
        {
            "id": "e98a0dfd-3f32-4538-b924-84afe0010cfa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6a2ad039-0066-44a5-815f-1171f3f88960",
            "compositeImage": {
                "id": "756ccb8d-9b8c-4dbb-8602-e70eeb981b33",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e98a0dfd-3f32-4538-b924-84afe0010cfa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3c41994c-cc77-425f-901f-9adc6b46ec0c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e98a0dfd-3f32-4538-b924-84afe0010cfa",
                    "LayerId": "aafc7fba-c164-4874-b9db-6fb02e70b4b0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "aafc7fba-c164-4874-b9db-6fb02e70b4b0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6a2ad039-0066-44a5-815f-1171f3f88960",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}