{
    "id": "f0c73ed9-aad6-48d9-bd04-c421ba4cd33d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player_ui_back",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 51,
    "bbox_left": 20,
    "bbox_right": 239,
    "bbox_top": 16,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c38eaad9-e5df-404a-9d59-1f99b0d6894b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f0c73ed9-aad6-48d9-bd04-c421ba4cd33d",
            "compositeImage": {
                "id": "3c7c1648-2a5e-4f11-8df9-efea989598b5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c38eaad9-e5df-404a-9d59-1f99b0d6894b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7dab0b27-dac3-4b04-9d50-e01054f87a88",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c38eaad9-e5df-404a-9d59-1f99b0d6894b",
                    "LayerId": "b8fd029d-3b0d-4242-8eaf-f36c1da37d89"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "b8fd029d-3b0d-4242-8eaf-f36c1da37d89",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f0c73ed9-aad6-48d9-bd04-c421ba4cd33d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 256,
    "xorig": 0,
    "yorig": 0
}