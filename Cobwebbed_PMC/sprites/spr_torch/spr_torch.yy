{
    "id": "d651c15f-1b8c-4491-8992-8fa1c00f2ca7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_torch",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 55,
    "bbox_left": 24,
    "bbox_right": 35,
    "bbox_top": 12,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d1099ecf-77ff-422b-86e1-65930eeb1643",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d651c15f-1b8c-4491-8992-8fa1c00f2ca7",
            "compositeImage": {
                "id": "15132529-7fab-4040-a765-df58b14f5f93",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d1099ecf-77ff-422b-86e1-65930eeb1643",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b5ecd4ac-ed8e-499b-8a81-68e73c4c8c7c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d1099ecf-77ff-422b-86e1-65930eeb1643",
                    "LayerId": "fe09d4a1-3a6e-41b0-9732-85bc35fe544a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "fe09d4a1-3a6e-41b0-9732-85bc35fe544a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d651c15f-1b8c-4491-8992-8fa1c00f2ca7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}