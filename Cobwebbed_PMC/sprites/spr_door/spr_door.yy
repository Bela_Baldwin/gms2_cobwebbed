{
    "id": "1eef3757-7146-4cd1-a92c-0060c00d2515",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_door",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d0fe7eac-6555-4056-8086-1c2d01b71a96",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1eef3757-7146-4cd1-a92c-0060c00d2515",
            "compositeImage": {
                "id": "45142b6b-52f7-4796-8daf-a7fd6cf1c3c6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d0fe7eac-6555-4056-8086-1c2d01b71a96",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f3775126-c6a8-42c8-a537-e71e1368f7a1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d0fe7eac-6555-4056-8086-1c2d01b71a96",
                    "LayerId": "c34f8a01-65c2-49f1-a54c-5b6d13ef3a0e"
                }
            ]
        },
        {
            "id": "286109c2-7d0d-413c-a305-151ffb5fd015",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1eef3757-7146-4cd1-a92c-0060c00d2515",
            "compositeImage": {
                "id": "ce5c0a4d-28ec-404f-8f59-595033aa42e8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "286109c2-7d0d-413c-a305-151ffb5fd015",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c174c1f9-f901-46b1-bd1d-16cbc71c85e7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "286109c2-7d0d-413c-a305-151ffb5fd015",
                    "LayerId": "c34f8a01-65c2-49f1-a54c-5b6d13ef3a0e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "c34f8a01-65c2-49f1-a54c-5b6d13ef3a0e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1eef3757-7146-4cd1-a92c-0060c00d2515",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}