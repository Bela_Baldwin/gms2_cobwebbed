{
    "id": "23efcfd6-bd4e-4f2e-8266-0753f7c6419f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_dungeon",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 383,
    "bbox_left": 4,
    "bbox_right": 511,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2e16b491-cdb3-46a3-993e-6c65edf1ed5e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "23efcfd6-bd4e-4f2e-8266-0753f7c6419f",
            "compositeImage": {
                "id": "4c93b864-61bf-4cc9-ba9d-3f8d616b736f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2e16b491-cdb3-46a3-993e-6c65edf1ed5e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0d3c9359-0f06-4a2b-9fee-e3dc67e8c27d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2e16b491-cdb3-46a3-993e-6c65edf1ed5e",
                    "LayerId": "41ce64fc-8ceb-411f-baf4-2d7bba8cbb67"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 384,
    "layers": [
        {
            "id": "41ce64fc-8ceb-411f-baf4-2d7bba8cbb67",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "23efcfd6-bd4e-4f2e-8266-0753f7c6419f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 512,
    "xorig": 0,
    "yorig": 0
}