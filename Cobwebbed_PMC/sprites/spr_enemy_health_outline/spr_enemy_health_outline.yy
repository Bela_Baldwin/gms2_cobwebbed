{
    "id": "6c30fc1c-cd86-449c-b18d-b08dd1142c8e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_enemy_health_outline",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 11,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9411748f-f08f-4742-8b41-8322a4dae9f2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6c30fc1c-cd86-449c-b18d-b08dd1142c8e",
            "compositeImage": {
                "id": "0d707b44-4387-4b0e-be87-5cf3982ff27d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9411748f-f08f-4742-8b41-8322a4dae9f2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a5239456-f763-4d29-9484-ae079931be3f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9411748f-f08f-4742-8b41-8322a4dae9f2",
                    "LayerId": "38355174-4288-4e77-9da7-1f5f4ffd634d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 12,
    "layers": [
        {
            "id": "38355174-4288-4e77-9da7-1f5f4ffd634d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6c30fc1c-cd86-449c-b18d-b08dd1142c8e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}