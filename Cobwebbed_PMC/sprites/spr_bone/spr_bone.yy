{
    "id": "0f8f1570-c62b-4e2e-baa3-914cd1ca8ce7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_bone",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 59,
    "bbox_left": 4,
    "bbox_right": 51,
    "bbox_top": 12,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "bf174eb0-102e-4d4c-a9e4-2a24026533eb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0f8f1570-c62b-4e2e-baa3-914cd1ca8ce7",
            "compositeImage": {
                "id": "0963283a-5ec6-47fc-a2d3-f530d7772a26",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bf174eb0-102e-4d4c-a9e4-2a24026533eb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5332eafb-cac9-48b4-a4fe-31d9681bc6e3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bf174eb0-102e-4d4c-a9e4-2a24026533eb",
                    "LayerId": "9f093ca5-b186-4eb1-bb1c-47f8bade5550"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "9f093ca5-b186-4eb1-bb1c-47f8bade5550",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0f8f1570-c62b-4e2e-baa3-914cd1ca8ce7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}