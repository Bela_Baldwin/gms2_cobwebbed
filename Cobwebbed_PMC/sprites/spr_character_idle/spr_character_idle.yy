{
    "id": "4e9b5702-3a71-44f8-b48e-2536a2d70087",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_character_idle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 55,
    "bbox_left": 12,
    "bbox_right": 55,
    "bbox_top": 8,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "fce9f4dc-12a3-44a4-97fa-08ab3abf7f82",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e9b5702-3a71-44f8-b48e-2536a2d70087",
            "compositeImage": {
                "id": "6f6d3fd6-e6f8-4ba8-adc0-7e51b7fa715d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fce9f4dc-12a3-44a4-97fa-08ab3abf7f82",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2acd2ccf-da2b-4445-99af-fa107e6823ee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fce9f4dc-12a3-44a4-97fa-08ab3abf7f82",
                    "LayerId": "339f07b9-b77a-44ee-85f4-89f8e7460795"
                }
            ]
        },
        {
            "id": "3cc3dd8b-1c8f-40a0-af21-cc023c26199b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4e9b5702-3a71-44f8-b48e-2536a2d70087",
            "compositeImage": {
                "id": "35fe21f8-ab5c-4e7a-8864-7c6fca667a5c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3cc3dd8b-1c8f-40a0-af21-cc023c26199b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eeed2ad3-1812-41f5-b70b-66194dcc40ba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3cc3dd8b-1c8f-40a0-af21-cc023c26199b",
                    "LayerId": "339f07b9-b77a-44ee-85f4-89f8e7460795"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "339f07b9-b77a-44ee-85f4-89f8e7460795",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4e9b5702-3a71-44f8-b48e-2536a2d70087",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 3,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}