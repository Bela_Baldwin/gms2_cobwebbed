{
    "id": "09d86c5f-26ec-439c-acd4-98eae221b5ed",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_livingAlter",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 8,
    "bbox_right": 55,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9dd0e2e7-21a9-40b5-928d-663bfb75c480",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "09d86c5f-26ec-439c-acd4-98eae221b5ed",
            "compositeImage": {
                "id": "3b417cbf-45d4-495d-8b2c-1e00da1115a9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9dd0e2e7-21a9-40b5-928d-663bfb75c480",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7968ea66-6655-4e37-85b1-a10ca8dda62c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9dd0e2e7-21a9-40b5-928d-663bfb75c480",
                    "LayerId": "1fb0bb89-21df-4ce9-a6a0-8825a78000f9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "1fb0bb89-21df-4ce9-a6a0-8825a78000f9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "09d86c5f-26ec-439c-acd4-98eae221b5ed",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}