{
    "id": "6675bdde-7131-49af-9128-de3e7b71d172",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_copper",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "68158447-1a99-4733-ad02-7c8b6958a28d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6675bdde-7131-49af-9128-de3e7b71d172",
            "compositeImage": {
                "id": "0b54ebf9-2357-41f6-b0af-be7bf7336199",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "68158447-1a99-4733-ad02-7c8b6958a28d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "df9648c7-3c28-4d5c-b14f-a30f5f0f19f3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "68158447-1a99-4733-ad02-7c8b6958a28d",
                    "LayerId": "893ff8f7-1a43-4039-b3a7-a609f8cc84ef"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "893ff8f7-1a43-4039-b3a7-a609f8cc84ef",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6675bdde-7131-49af-9128-de3e7b71d172",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}