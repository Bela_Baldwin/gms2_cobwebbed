{
    "id": "9b4fd5a9-e7a4-4a87-84f9-9cc1f70c8fd2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_zombie_walk_right",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 55,
    "bbox_left": 12,
    "bbox_right": 55,
    "bbox_top": 12,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0ad8208d-9083-4f53-a5ef-f81d306dfaf2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9b4fd5a9-e7a4-4a87-84f9-9cc1f70c8fd2",
            "compositeImage": {
                "id": "89b55364-2b00-4b1c-b8d3-e43ddb91e504",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0ad8208d-9083-4f53-a5ef-f81d306dfaf2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "70e659c4-aa7f-4536-bc30-e8b3400fc2d2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0ad8208d-9083-4f53-a5ef-f81d306dfaf2",
                    "LayerId": "3df3bfa7-c268-4980-a6f7-b3c07bb65b15"
                }
            ]
        },
        {
            "id": "5554a1c8-b8b6-4a85-83b0-1b706ce25530",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9b4fd5a9-e7a4-4a87-84f9-9cc1f70c8fd2",
            "compositeImage": {
                "id": "00cb83b2-0bb1-4add-80fc-94243327f995",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5554a1c8-b8b6-4a85-83b0-1b706ce25530",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "59be511c-705c-45fc-aecf-09142e7119a4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5554a1c8-b8b6-4a85-83b0-1b706ce25530",
                    "LayerId": "3df3bfa7-c268-4980-a6f7-b3c07bb65b15"
                }
            ]
        },
        {
            "id": "3fbc02f2-a9e6-4f2a-8916-0938fa8ec1ec",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9b4fd5a9-e7a4-4a87-84f9-9cc1f70c8fd2",
            "compositeImage": {
                "id": "973785a1-5ee5-4e58-8338-2a97b2aa48b3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3fbc02f2-a9e6-4f2a-8916-0938fa8ec1ec",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2807be45-79cd-4da1-998d-e80ea924a305",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3fbc02f2-a9e6-4f2a-8916-0938fa8ec1ec",
                    "LayerId": "3df3bfa7-c268-4980-a6f7-b3c07bb65b15"
                }
            ]
        },
        {
            "id": "7831b7bc-8e89-4069-8e16-4139f5641aaf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9b4fd5a9-e7a4-4a87-84f9-9cc1f70c8fd2",
            "compositeImage": {
                "id": "582c94a4-19f0-4828-9121-51c0415f36dc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7831b7bc-8e89-4069-8e16-4139f5641aaf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ee7ae2c7-6c89-4b73-bde2-6f085855ba8c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7831b7bc-8e89-4069-8e16-4139f5641aaf",
                    "LayerId": "3df3bfa7-c268-4980-a6f7-b3c07bb65b15"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "3df3bfa7-c268-4980-a6f7-b3c07bb65b15",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9b4fd5a9-e7a4-4a87-84f9-9cc1f70c8fd2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}