{
    "id": "69579a20-ed7e-47d7-b9c0-e0ba2f5c839d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player_health_outline",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 55,
    "bbox_left": 5,
    "bbox_right": 243,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "58c0a171-8e1c-4933-aad4-31a073bc0bd8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "69579a20-ed7e-47d7-b9c0-e0ba2f5c839d",
            "compositeImage": {
                "id": "1201c978-302e-4135-8494-de0e5d437aae",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "58c0a171-8e1c-4933-aad4-31a073bc0bd8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "84ce2b4a-ac89-4724-8ad4-570520449f59",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "58c0a171-8e1c-4933-aad4-31a073bc0bd8",
                    "LayerId": "c2893b16-e60a-4513-94c6-da4371a7e709"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "c2893b16-e60a-4513-94c6-da4371a7e709",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "69579a20-ed7e-47d7-b9c0-e0ba2f5c839d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 256,
    "xorig": 0,
    "yorig": 0
}