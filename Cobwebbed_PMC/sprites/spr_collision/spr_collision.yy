{
    "id": "bcb47e04-fe9d-4506-9229-ccd0ba6b1a50",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_collision",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "25d4ce1b-4ee4-4f55-a45f-df9e4993d08a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bcb47e04-fe9d-4506-9229-ccd0ba6b1a50",
            "compositeImage": {
                "id": "3c250e0d-f221-4565-aa97-1e6afa3e0227",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "25d4ce1b-4ee4-4f55-a45f-df9e4993d08a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dc165f85-f0aa-4d55-ae28-c70a7757c2e5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "25d4ce1b-4ee4-4f55-a45f-df9e4993d08a",
                    "LayerId": "586f5753-9211-4670-b5d2-4a86b06c4452"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "586f5753-9211-4670-b5d2-4a86b06c4452",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bcb47e04-fe9d-4506-9229-ccd0ba6b1a50",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}