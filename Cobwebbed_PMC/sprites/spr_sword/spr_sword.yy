{
    "id": "76092d6e-7a80-41f9-a2da-2f46165b480b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_sword",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 55,
    "bbox_left": 4,
    "bbox_right": 51,
    "bbox_top": 12,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "17275e30-5d31-425e-a7a3-f7951c6e9c91",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "76092d6e-7a80-41f9-a2da-2f46165b480b",
            "compositeImage": {
                "id": "6924ca31-0c1c-4c13-8ff8-5bfa54b6a7c9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "17275e30-5d31-425e-a7a3-f7951c6e9c91",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6b00ea71-da30-4147-ba74-94b5bfa91b9f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "17275e30-5d31-425e-a7a3-f7951c6e9c91",
                    "LayerId": "0bd9f067-a305-4608-949d-a73d51f713d9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "0bd9f067-a305-4608-949d-a73d51f713d9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "76092d6e-7a80-41f9-a2da-2f46165b480b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}