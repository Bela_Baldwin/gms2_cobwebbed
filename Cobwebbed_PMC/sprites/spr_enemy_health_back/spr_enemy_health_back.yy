{
    "id": "5b5f185e-fc1c-45c6-9296-db6aaa3760da",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_enemy_health_back",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 11,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "07822fa8-c190-4ff4-8e66-5e7e5048a598",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5b5f185e-fc1c-45c6-9296-db6aaa3760da",
            "compositeImage": {
                "id": "41b2a35c-fe51-4b2f-9868-b65b40f7fa0b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "07822fa8-c190-4ff4-8e66-5e7e5048a598",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "538d4656-2448-49cd-8256-8207391c62b5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "07822fa8-c190-4ff4-8e66-5e7e5048a598",
                    "LayerId": "f9687577-75f7-4ca5-97b3-6bd8dc41c6a0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 12,
    "layers": [
        {
            "id": "f9687577-75f7-4ca5-97b3-6bd8dc41c6a0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5b5f185e-fc1c-45c6-9296-db6aaa3760da",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}