{
    "id": "6a0d47dd-7915-4097-8351-21b723fb62dd",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_slot_head",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9e13543c-ef48-4801-a37a-4de2ff82f802",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "6a0d47dd-7915-4097-8351-21b723fb62dd",
            "compositeImage": {
                "id": "2da6f3c9-a8ca-4d8e-84e7-9c550a0dfcd7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9e13543c-ef48-4801-a37a-4de2ff82f802",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8172ccb9-3c0d-4c8c-be4a-75a8c454675e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9e13543c-ef48-4801-a37a-4de2ff82f802",
                    "LayerId": "bbab361a-0355-4607-93a8-f0ab6516c060"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "bbab361a-0355-4607-93a8-f0ab6516c060",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "6a0d47dd-7915-4097-8351-21b723fb62dd",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}