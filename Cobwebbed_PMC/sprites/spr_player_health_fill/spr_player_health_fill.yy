{
    "id": "341241e4-9c13-477b-8483-e0ec74bbfce9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player_health_fill",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 51,
    "bbox_left": 20,
    "bbox_right": 239,
    "bbox_top": 16,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "288d005e-f08a-41ca-9d8a-bbfa1e067132",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "341241e4-9c13-477b-8483-e0ec74bbfce9",
            "compositeImage": {
                "id": "775152a3-2615-4202-877e-006fe0d667d4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "288d005e-f08a-41ca-9d8a-bbfa1e067132",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "20a09fd1-0b3a-4daa-ae8d-e4cf7f390937",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "288d005e-f08a-41ca-9d8a-bbfa1e067132",
                    "LayerId": "58b04ed1-3fa5-4157-a020-66b9a412163f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "58b04ed1-3fa5-4157-a020-66b9a412163f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "341241e4-9c13-477b-8483-e0ec74bbfce9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 256,
    "xorig": 0,
    "yorig": 0
}