{
    "id": "ca65360c-d454-40dd-9d68-20bd9704b84e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_firepit",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 4,
    "bbox_right": 59,
    "bbox_top": 8,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a1e1ecd1-1bc3-46cb-9638-162344048adf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ca65360c-d454-40dd-9d68-20bd9704b84e",
            "compositeImage": {
                "id": "0a95e85d-8112-47bf-9252-88c69d070b7c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a1e1ecd1-1bc3-46cb-9638-162344048adf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4fc46d46-2a75-4406-8ee5-48470002b594",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a1e1ecd1-1bc3-46cb-9638-162344048adf",
                    "LayerId": "a3000197-2df9-4abb-bc03-d03ee1e2f876"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "a3000197-2df9-4abb-bc03-d03ee1e2f876",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ca65360c-d454-40dd-9d68-20bd9704b84e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}