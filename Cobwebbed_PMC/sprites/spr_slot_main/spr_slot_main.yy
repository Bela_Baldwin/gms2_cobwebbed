{
    "id": "d18ef78e-2874-4b2e-a37d-33064fee7221",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_slot_main",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9da561d0-33e5-4a65-9253-c314faa77e05",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d18ef78e-2874-4b2e-a37d-33064fee7221",
            "compositeImage": {
                "id": "378b7c9a-c139-4fa3-afc6-e67e69856522",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9da561d0-33e5-4a65-9253-c314faa77e05",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8007089d-45de-4bf5-9957-4226e1fb1547",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9da561d0-33e5-4a65-9253-c314faa77e05",
                    "LayerId": "a8b38ab5-9cdc-40b5-8826-dfb6e82e12c8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "a8b38ab5-9cdc-40b5-8826-dfb6e82e12c8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d18ef78e-2874-4b2e-a37d-33064fee7221",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}