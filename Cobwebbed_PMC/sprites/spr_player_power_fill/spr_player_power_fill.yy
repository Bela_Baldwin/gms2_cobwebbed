{
    "id": "299b5278-6453-4e9e-badd-f075ad7ee22c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player_power_fill",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 60,
    "bbox_left": 14,
    "bbox_right": 243,
    "bbox_top": 4,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3c997586-0923-42b6-a12a-06eac3334d1a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "299b5278-6453-4e9e-badd-f075ad7ee22c",
            "compositeImage": {
                "id": "0f20da2c-6f04-4fca-905d-d014381a3c41",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3c997586-0923-42b6-a12a-06eac3334d1a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "518070b5-cb0e-4af3-8f63-b02a627a528e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3c997586-0923-42b6-a12a-06eac3334d1a",
                    "LayerId": "f656e035-53ec-4f96-a5fa-dacef3c68243"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "f656e035-53ec-4f96-a5fa-dacef3c68243",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "299b5278-6453-4e9e-badd-f075ad7ee22c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 256,
    "xorig": 0,
    "yorig": 0
}