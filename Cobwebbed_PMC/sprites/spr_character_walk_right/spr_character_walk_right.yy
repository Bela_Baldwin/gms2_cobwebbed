{
    "id": "f712f5f6-c4ec-42d4-b16f-2d93ffd21879",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_character_walk_right",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 55,
    "bbox_left": 12,
    "bbox_right": 55,
    "bbox_top": 8,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "978254e9-1e4f-482d-bc42-550d3a9ace3b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f712f5f6-c4ec-42d4-b16f-2d93ffd21879",
            "compositeImage": {
                "id": "1e8f9528-4aa1-4e1d-8e4f-58b7d8464982",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "978254e9-1e4f-482d-bc42-550d3a9ace3b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f6aa0c21-61c3-4001-b883-02f4cf4ef4c5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "978254e9-1e4f-482d-bc42-550d3a9ace3b",
                    "LayerId": "9fcf148b-e811-4e44-bf54-8727294b8fb3"
                }
            ]
        },
        {
            "id": "86d61a35-9cb7-4443-9554-3879d52c9ce9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f712f5f6-c4ec-42d4-b16f-2d93ffd21879",
            "compositeImage": {
                "id": "950f7519-1dba-4106-b4ff-ce4a8c08ff00",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "86d61a35-9cb7-4443-9554-3879d52c9ce9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9fa8fcb4-b7c5-49f7-b08d-58c4c6aa53a4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "86d61a35-9cb7-4443-9554-3879d52c9ce9",
                    "LayerId": "9fcf148b-e811-4e44-bf54-8727294b8fb3"
                }
            ]
        },
        {
            "id": "cec2b1bc-5bf6-42e5-aafd-52c79504104a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f712f5f6-c4ec-42d4-b16f-2d93ffd21879",
            "compositeImage": {
                "id": "f20e95e3-8831-40fb-a22c-2830265af40c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cec2b1bc-5bf6-42e5-aafd-52c79504104a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e8724dce-8fdc-4b9e-8ea2-be2f615359c4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cec2b1bc-5bf6-42e5-aafd-52c79504104a",
                    "LayerId": "9fcf148b-e811-4e44-bf54-8727294b8fb3"
                }
            ]
        },
        {
            "id": "3fad930a-ca8d-4526-8fae-438d1d72f88e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f712f5f6-c4ec-42d4-b16f-2d93ffd21879",
            "compositeImage": {
                "id": "50db4573-562e-4d34-bdcc-cc18657f2e28",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3fad930a-ca8d-4526-8fae-438d1d72f88e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "31948bd4-3c23-47c2-a514-a665bf3179af",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3fad930a-ca8d-4526-8fae-438d1d72f88e",
                    "LayerId": "9fcf148b-e811-4e44-bf54-8727294b8fb3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "9fcf148b-e811-4e44-bf54-8727294b8fb3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f712f5f6-c4ec-42d4-b16f-2d93ffd21879",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}