{
    "id": "922b7c30-6019-4b9a-b5e4-121e2880a0e3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_silver",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c6bd1f36-bc72-48d7-bba8-fbbe61a3d22d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "922b7c30-6019-4b9a-b5e4-121e2880a0e3",
            "compositeImage": {
                "id": "dcb43c90-5f55-420d-a9ac-113709500a22",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c6bd1f36-bc72-48d7-bba8-fbbe61a3d22d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cc7bcfbd-e593-4c9f-95d9-4f77e36f0dd3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c6bd1f36-bc72-48d7-bba8-fbbe61a3d22d",
                    "LayerId": "6ca42ff4-a6ec-4d4f-be1c-4b2914cf0dc9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "6ca42ff4-a6ec-4d4f-be1c-4b2914cf0dc9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "922b7c30-6019-4b9a-b5e4-121e2880a0e3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}