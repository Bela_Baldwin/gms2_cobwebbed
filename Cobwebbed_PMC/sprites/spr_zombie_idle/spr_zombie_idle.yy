{
    "id": "52eca310-de8f-4328-a1fe-f0d72d4f10b5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_zombie_idle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 55,
    "bbox_left": 12,
    "bbox_right": 55,
    "bbox_top": 12,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6eca8415-e41d-4832-9ad9-1d586decdd08",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "52eca310-de8f-4328-a1fe-f0d72d4f10b5",
            "compositeImage": {
                "id": "55b0255e-73b4-45be-9918-a610cd88c6b9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6eca8415-e41d-4832-9ad9-1d586decdd08",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a451a4ea-97a1-4d00-b306-700d747838b3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6eca8415-e41d-4832-9ad9-1d586decdd08",
                    "LayerId": "90b388be-b7ad-47a0-8a38-9127662579f4"
                }
            ]
        },
        {
            "id": "12ab0df9-aadd-4978-ab9c-ffcacefc8d4f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "52eca310-de8f-4328-a1fe-f0d72d4f10b5",
            "compositeImage": {
                "id": "44c0c093-e473-43c4-80e3-a2ccf7874a16",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "12ab0df9-aadd-4978-ab9c-ffcacefc8d4f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c8256234-0d06-4e23-849e-906dafffcee3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "12ab0df9-aadd-4978-ab9c-ffcacefc8d4f",
                    "LayerId": "90b388be-b7ad-47a0-8a38-9127662579f4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "90b388be-b7ad-47a0-8a38-9127662579f4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "52eca310-de8f-4328-a1fe-f0d72d4f10b5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}