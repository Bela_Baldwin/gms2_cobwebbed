{
    "id": "2d1ca85c-80e2-41b9-a837-d482a8876f7a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_player_power_outline",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 60,
    "bbox_left": 14,
    "bbox_right": 243,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "338b2fbc-c18a-43bb-9489-9c08405fceaa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2d1ca85c-80e2-41b9-a837-d482a8876f7a",
            "compositeImage": {
                "id": "501a1e6f-96c3-42d3-8fd3-0e04ece21b3d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "338b2fbc-c18a-43bb-9489-9c08405fceaa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "540a36f9-c6cc-426b-a72a-50ca2e8920cb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "338b2fbc-c18a-43bb-9489-9c08405fceaa",
                    "LayerId": "f8119683-14c0-46b6-8d8b-c069b7be76fc"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "f8119683-14c0-46b6-8d8b-c069b7be76fc",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2d1ca85c-80e2-41b9-a837-d482a8876f7a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 256,
    "xorig": 0,
    "yorig": 0
}