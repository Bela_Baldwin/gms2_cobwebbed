{
    "id": "c87490c5-3fab-4565-bbeb-1871269fef27",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_enemy_health_fill",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 11,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "052c984e-586c-4734-94cd-cc12d73f70bf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c87490c5-3fab-4565-bbeb-1871269fef27",
            "compositeImage": {
                "id": "b85ef4d9-0339-4112-aeb5-5c244004caa6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "052c984e-586c-4734-94cd-cc12d73f70bf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c594cfd3-03ef-44dc-b8e2-92a55e948f68",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "052c984e-586c-4734-94cd-cc12d73f70bf",
                    "LayerId": "56389d89-409f-437b-a0ad-e4b0424bc4a7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 12,
    "layers": [
        {
            "id": "56389d89-409f-437b-a0ad-e4b0424bc4a7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c87490c5-3fab-4565-bbeb-1871269fef27",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}