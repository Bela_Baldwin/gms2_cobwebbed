{
    "id": "ced3dbf2-9932-4712-b4ed-0f82d311ef58",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_slot",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "36f781dc-9ee1-4f6e-a199-2b58d5c331bb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ced3dbf2-9932-4712-b4ed-0f82d311ef58",
            "compositeImage": {
                "id": "dbc5a27e-cf58-4652-a5dc-a1dbe7348993",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "36f781dc-9ee1-4f6e-a199-2b58d5c331bb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "39688917-df61-4744-9957-c2e6ae72072b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "36f781dc-9ee1-4f6e-a199-2b58d5c331bb",
                    "LayerId": "4d5816b9-8e29-4df2-8707-573385b93f74"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "4d5816b9-8e29-4df2-8707-573385b93f74",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ced3dbf2-9932-4712-b4ed-0f82d311ef58",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}