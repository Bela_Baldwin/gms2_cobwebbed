{
    "id": "406a81c8-d5c5-458e-9ed5-df7489f5c8ae",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_slot_body",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b109e4b1-4dfe-48e0-b627-ef71f59f8f8f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "406a81c8-d5c5-458e-9ed5-df7489f5c8ae",
            "compositeImage": {
                "id": "48286270-c709-4343-8620-47698021c26c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b109e4b1-4dfe-48e0-b627-ef71f59f8f8f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b4ad326d-3375-4a0a-8660-1a9732c6f9d8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b109e4b1-4dfe-48e0-b627-ef71f59f8f8f",
                    "LayerId": "612c9af3-f07c-465e-925d-c0f1c6683e45"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "612c9af3-f07c-465e-925d-c0f1c6683e45",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "406a81c8-d5c5-458e-9ed5-df7489f5c8ae",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}