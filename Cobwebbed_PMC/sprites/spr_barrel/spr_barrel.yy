{
    "id": "1d8763c1-bf27-400b-9576-473d21e6e3f6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_barrel",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 59,
    "bbox_left": 12,
    "bbox_right": 51,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a0bfe0c1-5b3e-45e8-b7d3-1b26a9977622",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1d8763c1-bf27-400b-9576-473d21e6e3f6",
            "compositeImage": {
                "id": "bc9dc13d-dbf6-4cf2-9534-383a54071026",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a0bfe0c1-5b3e-45e8-b7d3-1b26a9977622",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dc16e690-051f-4816-b73f-17a119f1b061",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a0bfe0c1-5b3e-45e8-b7d3-1b26a9977622",
                    "LayerId": "a44b5196-ad67-4f53-8252-e440a323b525"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "a44b5196-ad67-4f53-8252-e440a323b525",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1d8763c1-bf27-400b-9576-473d21e6e3f6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}