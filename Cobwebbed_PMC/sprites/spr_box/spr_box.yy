{
    "id": "d4f731d8-4e48-43d3-baf0-3ec42d6dc6e2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_box",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 59,
    "bbox_left": 12,
    "bbox_right": 51,
    "bbox_top": 8,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "370c061a-1c11-4ac7-82fb-22a90f6dd8da",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d4f731d8-4e48-43d3-baf0-3ec42d6dc6e2",
            "compositeImage": {
                "id": "1263bed2-6ec2-4feb-afa9-084feb908d17",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "370c061a-1c11-4ac7-82fb-22a90f6dd8da",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7138a3d3-6a42-4a1e-ad82-b00ccee61885",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "370c061a-1c11-4ac7-82fb-22a90f6dd8da",
                    "LayerId": "9a6ed9bd-2b2c-4c3c-bcc3-233f0bf4116c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "9a6ed9bd-2b2c-4c3c-bcc3-233f0bf4116c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d4f731d8-4e48-43d3-baf0-3ec42d6dc6e2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}