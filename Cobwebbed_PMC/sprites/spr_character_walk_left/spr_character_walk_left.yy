{
    "id": "7c94a5d9-5b7e-4c8c-a49c-c7b947e77c08",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_character_walk_left",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 55,
    "bbox_left": 12,
    "bbox_right": 55,
    "bbox_top": 8,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e531ba79-329c-470c-809a-0b5b4be9b4c9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c94a5d9-5b7e-4c8c-a49c-c7b947e77c08",
            "compositeImage": {
                "id": "c6343995-fac9-41a4-9e16-0680b8136d3c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e531ba79-329c-470c-809a-0b5b4be9b4c9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f6e6b9eb-1f20-4877-a451-fb39366f3f1b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e531ba79-329c-470c-809a-0b5b4be9b4c9",
                    "LayerId": "5d7fb290-78a6-4da0-8b48-a183e4e949ef"
                }
            ]
        },
        {
            "id": "661eb9dd-7123-4ba3-8c20-c8a03e6b6a35",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c94a5d9-5b7e-4c8c-a49c-c7b947e77c08",
            "compositeImage": {
                "id": "fc822de7-a34a-40ab-84c0-e2cb1cd6dc4c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "661eb9dd-7123-4ba3-8c20-c8a03e6b6a35",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8bb1d1f6-b879-41f3-bfec-4eb48074fb8d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "661eb9dd-7123-4ba3-8c20-c8a03e6b6a35",
                    "LayerId": "5d7fb290-78a6-4da0-8b48-a183e4e949ef"
                }
            ]
        },
        {
            "id": "9c46bad5-51ac-43aa-a9d1-909f6d794b9b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c94a5d9-5b7e-4c8c-a49c-c7b947e77c08",
            "compositeImage": {
                "id": "1b8ba979-e195-4cc1-b13c-68ef0fccaab6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9c46bad5-51ac-43aa-a9d1-909f6d794b9b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6255caef-a3cd-43c8-8a61-d22b18df21eb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9c46bad5-51ac-43aa-a9d1-909f6d794b9b",
                    "LayerId": "5d7fb290-78a6-4da0-8b48-a183e4e949ef"
                }
            ]
        },
        {
            "id": "7e781b77-8be5-45a3-91f1-07ba18b40675",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7c94a5d9-5b7e-4c8c-a49c-c7b947e77c08",
            "compositeImage": {
                "id": "5ea375b9-f032-4ed7-bff2-d8473caf41fe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7e781b77-8be5-45a3-91f1-07ba18b40675",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f6c78415-de77-4e5c-b15c-f05db63497ba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7e781b77-8be5-45a3-91f1-07ba18b40675",
                    "LayerId": "5d7fb290-78a6-4da0-8b48-a183e4e949ef"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "5d7fb290-78a6-4da0-8b48-a183e4e949ef",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7c94a5d9-5b7e-4c8c-a49c-c7b947e77c08",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}