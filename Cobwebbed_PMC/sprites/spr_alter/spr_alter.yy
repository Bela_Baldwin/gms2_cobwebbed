{
    "id": "d7a4edad-52eb-4fe7-91d2-6dc0c203a5a0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_alter",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 8,
    "bbox_right": 55,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6f173b79-2158-45e7-9077-00ddf000b69c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d7a4edad-52eb-4fe7-91d2-6dc0c203a5a0",
            "compositeImage": {
                "id": "6f0aab2b-3c2b-4f84-a835-8da6d83652d6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6f173b79-2158-45e7-9077-00ddf000b69c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c744edab-b325-487f-8d6b-82c0d2c86b71",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6f173b79-2158-45e7-9077-00ddf000b69c",
                    "LayerId": "f6aa421e-70a7-42cd-a4ef-65d860d0a718"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "f6aa421e-70a7-42cd-a4ef-65d860d0a718",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d7a4edad-52eb-4fe7-91d2-6dc0c203a5a0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}