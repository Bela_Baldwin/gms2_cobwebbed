{
    "id": "35a1a25a-e377-4190-9599-dc98f04f19b7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "spr_parchment",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 12,
    "bbox_right": 51,
    "bbox_top": 28,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3c258394-4481-4ed9-b8a4-d67248f4a665",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "35a1a25a-e377-4190-9599-dc98f04f19b7",
            "compositeImage": {
                "id": "bb1b3426-e3d9-4ab6-b736-fbfc48ac58ac",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3c258394-4481-4ed9-b8a4-d67248f4a665",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "694c0db0-df80-493b-829c-455d89126b7d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3c258394-4481-4ed9-b8a4-d67248f4a665",
                    "LayerId": "0b7e1278-04d9-4880-98e8-e01e84aeecc8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "0b7e1278-04d9-4880-98e8-e01e84aeecc8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "35a1a25a-e377-4190-9599-dc98f04f19b7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 0,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}