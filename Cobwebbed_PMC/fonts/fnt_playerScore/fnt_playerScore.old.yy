{
    "id": "d6ceb9bc-3a70-4ed1-916f-2d1fb413164d",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fnt_playerScore",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Parchment",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "2be18bcc-879a-4750-8add-b97f9d46976f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 142,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "96597c1d-2466-4fd5-80c1-fd869e5b3449",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 142,
                "offset": 3,
                "shift": 15,
                "w": 11,
                "x": 927,
                "y": 290
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "b323be37-301c-47f4-a3ca-a3ff5c37d22d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 142,
                "offset": -1,
                "shift": 14,
                "w": 16,
                "x": 909,
                "y": 290
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "011a59ec-3202-4197-ab3f-fcadb532b0b0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 142,
                "offset": 0,
                "shift": 29,
                "w": 28,
                "x": 879,
                "y": 290
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "7bcfd57b-ae1a-4e41-84f5-c4eb3383b679",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 142,
                "offset": 0,
                "shift": 29,
                "w": 28,
                "x": 849,
                "y": 290
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "c76e3ea1-109d-45be-bf06-14c7309fff56",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 142,
                "offset": 2,
                "shift": 48,
                "w": 45,
                "x": 802,
                "y": 290
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "242cbf2c-5a86-401b-ba6a-8de1c117933d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 142,
                "offset": 2,
                "shift": 37,
                "w": 35,
                "x": 765,
                "y": 290
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "e4c43e09-1c07-41e2-90de-0aaf2d8a07de",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 142,
                "offset": -1,
                "shift": 5,
                "w": 8,
                "x": 755,
                "y": 290
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "bb4b1bc4-377d-4d94-b18f-35385d4e229e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 142,
                "offset": 3,
                "shift": 25,
                "w": 20,
                "x": 733,
                "y": 290
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "ea3c96b1-0853-48a1-92b7-eeffb1a4b5d0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 142,
                "offset": 2,
                "shift": 25,
                "w": 21,
                "x": 710,
                "y": 290
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "a724aa49-f610-4803-829b-dccf0cf2c9d2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 142,
                "offset": -5,
                "shift": 15,
                "w": 19,
                "x": 940,
                "y": 290
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "bef93527-129d-40cf-a0e5-db3b9a15bcbd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 142,
                "offset": 0,
                "shift": 22,
                "w": 23,
                "x": 685,
                "y": 290
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "e14b9698-1a89-48dc-9903-23a36958bd54",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 142,
                "offset": -1,
                "shift": 11,
                "w": 12,
                "x": 644,
                "y": 290
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "99c46b0a-3b63-4b6c-ad1b-34b6974e19ec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 142,
                "offset": 0,
                "shift": 28,
                "w": 28,
                "x": 614,
                "y": 290
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "62713349-7455-43c7-ba81-3092727bc6e2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 142,
                "offset": 3,
                "shift": 14,
                "w": 8,
                "x": 604,
                "y": 290
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "d7c882c0-210f-4f62-854e-a44e47672aea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 142,
                "offset": -6,
                "shift": 25,
                "w": 36,
                "x": 566,
                "y": 290
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "3bd90777-8c01-47be-a5b9-8722040344ca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 142,
                "offset": 3,
                "shift": 27,
                "w": 22,
                "x": 542,
                "y": 290
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "796690d1-74d2-4867-84f5-5a175fc73d87",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 142,
                "offset": -1,
                "shift": 18,
                "w": 19,
                "x": 521,
                "y": 290
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "2c8729a6-7320-4ba0-b018-604ffe440694",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 142,
                "offset": -1,
                "shift": 24,
                "w": 24,
                "x": 495,
                "y": 290
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "668a222e-6890-421f-b383-898da851d0f8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 142,
                "offset": -2,
                "shift": 22,
                "w": 24,
                "x": 469,
                "y": 290
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "0de2cdd8-0b4d-49a3-b6be-80341cf595b8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 142,
                "offset": -2,
                "shift": 26,
                "w": 29,
                "x": 438,
                "y": 290
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "935495d0-2046-408c-b61a-8415b0b908c5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 142,
                "offset": 0,
                "shift": 24,
                "w": 25,
                "x": 658,
                "y": 290
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "f46b9fd8-659d-4367-9d32-cc321c50f957",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 142,
                "offset": 4,
                "shift": 28,
                "w": 23,
                "x": 961,
                "y": 290
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "17768dc8-1900-4263-9e8d-0c1dd22bad78",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 142,
                "offset": -2,
                "shift": 22,
                "w": 26,
                "x": 986,
                "y": 290
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "9623eb5b-1499-44ab-9da7-0c41a67c3e69",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 142,
                "offset": 2,
                "shift": 26,
                "w": 23,
                "x": 2,
                "y": 434
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "7a4030c5-e2b5-4c62-9311-b0357900466f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 142,
                "offset": 2,
                "shift": 28,
                "w": 24,
                "x": 602,
                "y": 578
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "ebf8d6e3-0964-42d3-899f-11c38244f112",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 142,
                "offset": 5,
                "shift": 15,
                "w": 8,
                "x": 592,
                "y": 578
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "fba1de5c-06ca-4e6f-b518-30b417c03e2e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 142,
                "offset": 2,
                "shift": 15,
                "w": 13,
                "x": 577,
                "y": 578
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "94a787d6-8aad-4183-9956-fe4a8ba76c15",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 142,
                "offset": 1,
                "shift": 24,
                "w": 21,
                "x": 554,
                "y": 578
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "0207e29b-9e5d-4935-a7a8-2be024e37aae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 142,
                "offset": 0,
                "shift": 22,
                "w": 22,
                "x": 530,
                "y": 578
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "6499c1e2-9a8b-4933-a274-1355956290c5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 142,
                "offset": 2,
                "shift": 24,
                "w": 20,
                "x": 508,
                "y": 578
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "81756d0b-7b13-4e06-8bd2-bdf9864122d1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 142,
                "offset": -3,
                "shift": 24,
                "w": 24,
                "x": 482,
                "y": 578
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "801bb3f0-4f9e-44e0-a41e-b1b6ee1595cd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 142,
                "offset": 2,
                "shift": 51,
                "w": 46,
                "x": 434,
                "y": 578
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "4a483868-70de-46b3-9609-7768c8a26c74",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 142,
                "offset": 1,
                "shift": 123,
                "w": 122,
                "x": 310,
                "y": 578
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "8f37240d-5b9a-4c48-b42b-38d07db6f5ef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 142,
                "offset": 0,
                "shift": 109,
                "w": 109,
                "x": 199,
                "y": 578
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "26045980-73bd-4f16-981a-1e9d8c2a0b85",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 142,
                "offset": 0,
                "shift": 94,
                "w": 97,
                "x": 100,
                "y": 578
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "acd612b6-1378-44f9-83cc-867e152dc993",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 142,
                "offset": 0,
                "shift": 94,
                "w": 96,
                "x": 2,
                "y": 578
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "e16f769d-20e9-45ab-87f7-2943588297ac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 142,
                "offset": 0,
                "shift": 92,
                "w": 97,
                "x": 852,
                "y": 434
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "cfaebd5b-86b7-4eb7-a53e-0fc2061a0d20",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 142,
                "offset": 0,
                "shift": 82,
                "w": 88,
                "x": 762,
                "y": 434
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "745a4407-3a5d-466d-85f9-c1ba70d7a607",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 142,
                "offset": 0,
                "shift": 127,
                "w": 126,
                "x": 634,
                "y": 434
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "7fb36e6f-ad05-4f80-971d-b6fcf5e72d38",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 142,
                "offset": 0,
                "shift": 100,
                "w": 101,
                "x": 531,
                "y": 434
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "1216e1b3-583c-4fc5-8636-ac44d939a202",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 142,
                "offset": 0,
                "shift": 81,
                "w": 83,
                "x": 446,
                "y": 434
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "cbfa0014-7f4a-44d2-80e0-161add9d0b08",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 142,
                "offset": 0,
                "shift": 81,
                "w": 83,
                "x": 361,
                "y": 434
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "4b4cba70-084a-4b6e-86b1-e40079eb7801",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 142,
                "offset": 0,
                "shift": 102,
                "w": 103,
                "x": 256,
                "y": 434
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "71ccc933-aefc-42c1-8f93-c161f5b7c892",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 142,
                "offset": 0,
                "shift": 102,
                "w": 101,
                "x": 153,
                "y": 434
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "ffd70656-5398-4db7-a523-35c88560f9ed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 142,
                "offset": 0,
                "shift": 124,
                "w": 124,
                "x": 27,
                "y": 434
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "dd2517fd-ea3c-49a5-8fe0-9497dc0ff76d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 142,
                "offset": 0,
                "shift": 118,
                "w": 121,
                "x": 315,
                "y": 290
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "83357422-2fde-4f70-8a09-d8c5a678bcab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 142,
                "offset": 0,
                "shift": 114,
                "w": 113,
                "x": 200,
                "y": 290
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "13b736ee-7143-4ac6-a7bb-9b5afa3ceee7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 142,
                "offset": 0,
                "shift": 107,
                "w": 107,
                "x": 91,
                "y": 290
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "d047a5fe-a4a3-4060-bd0b-922e9f436ecb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 142,
                "offset": 0,
                "shift": 106,
                "w": 106,
                "x": 318,
                "y": 146
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "c11a594c-9b31-4314-b33c-01f919ebef56",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 142,
                "offset": 0,
                "shift": 96,
                "w": 99,
                "x": 197,
                "y": 146
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "197b371b-277d-45ef-a1d0-63492b01c122",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 142,
                "offset": 0,
                "shift": 96,
                "w": 95,
                "x": 100,
                "y": 146
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "f0ce183d-5ff7-4f8d-b047-d59d0479ed12",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 142,
                "offset": 0,
                "shift": 95,
                "w": 96,
                "x": 2,
                "y": 146
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "68b4baea-b0f1-45a6-84e5-f00f531f180f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 142,
                "offset": 0,
                "shift": 100,
                "w": 101,
                "x": 864,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "b0419a8b-f0ab-421e-ac68-09eae78877c8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 142,
                "offset": 0,
                "shift": 91,
                "w": 93,
                "x": 769,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "14ceba8f-d838-46e2-b0ba-2283763f02c4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 142,
                "offset": 0,
                "shift": 118,
                "w": 120,
                "x": 647,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "ed0640f3-977d-42ca-a3c6-90a2f7ab677d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 142,
                "offset": 0,
                "shift": 92,
                "w": 104,
                "x": 541,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "ded7163a-eb16-4555-a245-f40f598480dc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 142,
                "offset": 0,
                "shift": 97,
                "w": 100,
                "x": 439,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "81156b0d-6aa6-49f7-86f7-5bc6e09b5bb2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 142,
                "offset": 0,
                "shift": 100,
                "w": 98,
                "x": 339,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "4ffd2750-f088-48ce-adb4-9a5210c02b80",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 142,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 298,
                "y": 146
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "2811bfeb-7408-43f2-8a6b-004d92c83ab8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 142,
                "offset": -6,
                "shift": 25,
                "w": 39,
                "x": 298,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "598b0b9d-8e94-4e20-8dbb-1b6de455b58f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 142,
                "offset": 2,
                "shift": 18,
                "w": 17,
                "x": 251,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "73288cde-6ed8-4d2c-8b1e-14c8fb1bb174",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 142,
                "offset": 1,
                "shift": 41,
                "w": 39,
                "x": 210,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "60262b95-ce78-456a-af79-3a9414718375",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 142,
                "offset": -1,
                "shift": 47,
                "w": 49,
                "x": 159,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "1dd4fc58-8c87-426b-a6e2-cc81ec196226",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 142,
                "offset": 10,
                "shift": 33,
                "w": 14,
                "x": 143,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "1c21436c-3b1f-4fa2-b91e-af36ae6a3248",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 142,
                "offset": 2,
                "shift": 28,
                "w": 28,
                "x": 113,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "a795d20a-34b6-4cdf-a206-31c3bbb2d33b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 142,
                "offset": 1,
                "shift": 31,
                "w": 27,
                "x": 84,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "2238ed81-4204-46d9-8322-654447e39574",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 142,
                "offset": 2,
                "shift": 21,
                "w": 20,
                "x": 62,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "8ae73134-d932-4735-b9b4-1c1bbd377bea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 142,
                "offset": 2,
                "shift": 26,
                "w": 24,
                "x": 36,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "5e377a81-299d-4767-8cbd-8236f357c32b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 142,
                "offset": 2,
                "shift": 21,
                "w": 18,
                "x": 16,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "d1c3ff47-29b4-48d2-9542-bfc7b8902cfe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 142,
                "offset": 2,
                "shift": 17,
                "w": 26,
                "x": 270,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "9ab04e49-b1f3-4553-b6a3-34270e559c80",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 142,
                "offset": 2,
                "shift": 28,
                "w": 27,
                "x": 426,
                "y": 146
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "2952372c-2a60-47d7-b41f-86132f56e046",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 142,
                "offset": 0,
                "shift": 31,
                "w": 30,
                "x": 724,
                "y": 146
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "93c4c45a-272e-43ad-b466-e639342f92de",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 142,
                "offset": -1,
                "shift": 16,
                "w": 18,
                "x": 455,
                "y": 146
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "d5c6d156-bee1-4aa1-a5b1-2d611b3c26a6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 142,
                "offset": -2,
                "shift": 18,
                "w": 20,
                "x": 36,
                "y": 290
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "7aa2c3d0-8f04-437a-ab07-874e7ed5882d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 142,
                "offset": 2,
                "shift": 32,
                "w": 32,
                "x": 2,
                "y": 290
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "778b6392-4e17-47f6-9fec-9cb074728464",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 142,
                "offset": 0,
                "shift": 17,
                "w": 27,
                "x": 980,
                "y": 146
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "2b23eeb6-6113-443b-91f9-b3a44675bea7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 142,
                "offset": -2,
                "shift": 45,
                "w": 49,
                "x": 929,
                "y": 146
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "34396fc6-0fd0-4653-914d-7676f7982f1b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 142,
                "offset": -1,
                "shift": 31,
                "w": 34,
                "x": 893,
                "y": 146
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "5ab00aa5-452e-4302-a8d8-081c28191629",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 142,
                "offset": 2,
                "shift": 26,
                "w": 22,
                "x": 869,
                "y": 146
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "1a73b760-8baa-43b4-bd4d-dea6a9b918d6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 142,
                "offset": -3,
                "shift": 30,
                "w": 33,
                "x": 834,
                "y": 146
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "b4326db4-2ccc-43a7-bac3-b60e6334c701",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 142,
                "offset": 2,
                "shift": 29,
                "w": 26,
                "x": 806,
                "y": 146
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "bcc3ea85-b198-4b65-bd79-c8bcd0f0f2a3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 142,
                "offset": -1,
                "shift": 24,
                "w": 27,
                "x": 777,
                "y": 146
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "1d931734-e0d8-4605-83ab-41e3704d41d4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 142,
                "offset": -3,
                "shift": 29,
                "w": 31,
                "x": 58,
                "y": 290
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "87be8827-52c5-4e77-a4f8-81c405fa5985",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 142,
                "offset": 0,
                "shift": 19,
                "w": 19,
                "x": 756,
                "y": 146
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "8eb10641-5607-4456-b91f-820d26b8a10d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 142,
                "offset": -1,
                "shift": 31,
                "w": 33,
                "x": 689,
                "y": 146
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "bdcda9a3-a65b-4885-b6a9-5ed20ca6bec4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 142,
                "offset": -1,
                "shift": 29,
                "w": 30,
                "x": 657,
                "y": 146
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "7932c130-817a-46e1-889f-53eb82bed237",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 142,
                "offset": -1,
                "shift": 42,
                "w": 42,
                "x": 613,
                "y": 146
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "5efeb359-0980-4561-b4ca-4fe90c386536",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 142,
                "offset": -5,
                "shift": 23,
                "w": 30,
                "x": 581,
                "y": 146
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "120c3bca-27a3-461d-8d54-6f019759d0e5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 142,
                "offset": -1,
                "shift": 30,
                "w": 31,
                "x": 548,
                "y": 146
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "176c1b99-f706-4139-a19f-c4055b5a222b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 142,
                "offset": -1,
                "shift": 18,
                "w": 16,
                "x": 530,
                "y": 146
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "3bbe3347-2fae-4520-9a09-66888958b7dd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 142,
                "offset": 1,
                "shift": 25,
                "w": 22,
                "x": 506,
                "y": 146
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "eac6f77a-86e7-441c-9367-c0416256f4ee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 142,
                "offset": 6,
                "shift": 18,
                "w": 5,
                "x": 499,
                "y": 146
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "b06cc790-ec63-4dc7-bf21-ebf891bd6661",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 142,
                "offset": 4,
                "shift": 26,
                "w": 22,
                "x": 475,
                "y": 146
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "630d40f4-cf7a-44a2-88d2-00ee0f543443",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 142,
                "offset": 0,
                "shift": 32,
                "w": 30,
                "x": 628,
                "y": 578
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "50b4c189-4ffc-47af-93e6-5b6eff6a332d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 142,
                "offset": 25,
                "shift": 80,
                "w": 30,
                "x": 660,
                "y": 578
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)",
    "size": 100,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}