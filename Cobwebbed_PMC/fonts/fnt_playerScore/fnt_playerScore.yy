{
    "id": "d6ceb9bc-3a70-4ed1-916f-2d1fb413164d",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "fnt_playerScore",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Parchment",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "51df702a-f919-48ad-b195-6248c49d05b3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 114,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "b46cdb96-8589-4455-8308-6d27300944a3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 114,
                "offset": 2,
                "shift": 12,
                "w": 9,
                "x": 407,
                "y": 234
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "cc8d3c81-ff80-4ca2-997e-d406889e8685",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 114,
                "offset": -1,
                "shift": 11,
                "w": 13,
                "x": 392,
                "y": 234
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "32bb4a5b-f968-427f-b7ed-122813da04da",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 114,
                "offset": 0,
                "shift": 23,
                "w": 22,
                "x": 368,
                "y": 234
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "f9921716-7650-4b4e-b0a9-d087a82d4666",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 114,
                "offset": 0,
                "shift": 23,
                "w": 23,
                "x": 343,
                "y": 234
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "4ec9e742-151e-4dc8-9a42-c347d14a44cd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 114,
                "offset": 1,
                "shift": 38,
                "w": 37,
                "x": 304,
                "y": 234
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "223bf9e5-0c95-4617-9950-fc521d45527f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 114,
                "offset": 1,
                "shift": 30,
                "w": 29,
                "x": 273,
                "y": 234
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "4f290e8b-9e35-4991-a3e0-a2370e499cf5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 114,
                "offset": -1,
                "shift": 4,
                "w": 7,
                "x": 264,
                "y": 234
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "03819937-9f90-4685-9cb5-26f6f3d02b0c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 114,
                "offset": 2,
                "shift": 20,
                "w": 17,
                "x": 245,
                "y": 234
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "d512ded7-9abd-419a-860e-49b00ae57ede",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 114,
                "offset": 2,
                "shift": 20,
                "w": 16,
                "x": 227,
                "y": 234
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "d7a7ab60-fa0d-4043-89cb-11a36772bfb1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 114,
                "offset": -4,
                "shift": 12,
                "w": 15,
                "x": 418,
                "y": 234
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "22530317-3a13-4692-a6e0-a66debd9b445",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 114,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 207,
                "y": 234
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "e3da0128-4aff-44a8-af91-9751028a094b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 114,
                "offset": -1,
                "shift": 9,
                "w": 10,
                "x": 173,
                "y": 234
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "edd01697-6238-4a20-9365-d8f5ae32cdda",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 114,
                "offset": 0,
                "shift": 22,
                "w": 22,
                "x": 149,
                "y": 234
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "03ebfab1-6e7f-4003-ba5f-b623a7fac386",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 114,
                "offset": 2,
                "shift": 12,
                "w": 7,
                "x": 140,
                "y": 234
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "20319412-5e9e-4edb-847c-3691025368f5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 114,
                "offset": -5,
                "shift": 20,
                "w": 29,
                "x": 109,
                "y": 234
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "11f2c04b-7374-4812-99dd-d604e7b82650",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 114,
                "offset": 2,
                "shift": 22,
                "w": 18,
                "x": 89,
                "y": 234
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "b862d893-cf9f-4758-87e3-d86a9cec0a3c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 114,
                "offset": -1,
                "shift": 15,
                "w": 15,
                "x": 72,
                "y": 234
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "9b035620-b77c-49b0-a0b1-623545fd16fb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 114,
                "offset": -1,
                "shift": 19,
                "w": 20,
                "x": 50,
                "y": 234
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "1320eb8e-35e4-4b40-b3ac-dea5a3f0f159",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 114,
                "offset": -2,
                "shift": 18,
                "w": 20,
                "x": 28,
                "y": 234
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "f7426248-c2a2-4a74-b781-713fbe75a638",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 114,
                "offset": -2,
                "shift": 21,
                "w": 24,
                "x": 2,
                "y": 234
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "32136911-85e0-4790-a4ab-62dd47d555ca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 114,
                "offset": 0,
                "shift": 19,
                "w": 20,
                "x": 185,
                "y": 234
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "8f001c92-1c30-4dca-a08f-1abe011ea28d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 114,
                "offset": 3,
                "shift": 22,
                "w": 19,
                "x": 435,
                "y": 234
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "e3ee1149-4b27-443e-a10b-4c350b34d786",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 114,
                "offset": -2,
                "shift": 18,
                "w": 21,
                "x": 456,
                "y": 234
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "6d0b2ab9-ae96-41df-b7ce-323ae341ef28",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 114,
                "offset": 2,
                "shift": 21,
                "w": 18,
                "x": 479,
                "y": 234
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "d11ef2da-07b8-4eee-b678-02b1cf96fd49",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 114,
                "offset": 2,
                "shift": 22,
                "w": 19,
                "x": 750,
                "y": 350
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "f8ff5ca2-f31c-4f5f-b4f7-876b213c0b9d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 114,
                "offset": 4,
                "shift": 12,
                "w": 7,
                "x": 741,
                "y": 350
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "1fa5c373-0132-4e7a-a28e-f2535e9649f7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 114,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 728,
                "y": 350
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "ab58db02-d3f5-45c8-a5b9-f90985e8311c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 114,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 709,
                "y": 350
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "4b78b3d7-4536-4bd6-9e8b-0dea433dfbcb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 114,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 689,
                "y": 350
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "59a0bc44-0e62-47c2-8748-c3fc324b5960",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 114,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 670,
                "y": 350
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "ad174dc0-9421-47e0-8f65-8db0a9d50d97",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 114,
                "offset": -3,
                "shift": 19,
                "w": 20,
                "x": 648,
                "y": 350
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "a7ff5287-a718-4d4f-ac89-002860ae5f0a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 114,
                "offset": 1,
                "shift": 41,
                "w": 37,
                "x": 609,
                "y": 350
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "b7df07e7-edba-40a5-a607-2cd3b77c09ae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 114,
                "offset": 0,
                "shift": 99,
                "w": 99,
                "x": 508,
                "y": 350
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "c22857c5-f355-483e-b2eb-0c3393befa27",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 114,
                "offset": 0,
                "shift": 88,
                "w": 88,
                "x": 418,
                "y": 350
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "e9066629-3439-4ecc-a6a6-7da25ab0eea0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 114,
                "offset": 0,
                "shift": 76,
                "w": 78,
                "x": 338,
                "y": 350
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "c2f5ea0a-b031-42f6-b115-a4e946a449c5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 114,
                "offset": 0,
                "shift": 76,
                "w": 78,
                "x": 258,
                "y": 350
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "2e559987-5149-4f3c-bb9d-550455663ff1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 114,
                "offset": 0,
                "shift": 74,
                "w": 78,
                "x": 178,
                "y": 350
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "9e3820d3-040d-454b-9c53-5d9c89b4e73b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 114,
                "offset": 0,
                "shift": 66,
                "w": 71,
                "x": 105,
                "y": 350
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "61d7986d-6bc4-4cd3-835b-77ab02a011d1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 114,
                "offset": 0,
                "shift": 102,
                "w": 101,
                "x": 2,
                "y": 350
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "1d31d3d6-7730-40d9-8030-3a6a7bc133b0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 114,
                "offset": 0,
                "shift": 80,
                "w": 81,
                "x": 907,
                "y": 234
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "78c3458b-e526-4cad-9d1b-9c7e5653aeaf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 114,
                "offset": 0,
                "shift": 65,
                "w": 67,
                "x": 838,
                "y": 234
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "e6337639-693c-4b2f-b4d8-1389a7b7d609",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 114,
                "offset": 0,
                "shift": 65,
                "w": 67,
                "x": 769,
                "y": 234
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "5c582a7b-b9df-4219-a6ed-7fa81df6d438",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 114,
                "offset": 0,
                "shift": 82,
                "w": 83,
                "x": 684,
                "y": 234
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "336ebb2c-a4f7-4e91-8371-93cf4d01a232",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 114,
                "offset": 0,
                "shift": 82,
                "w": 81,
                "x": 601,
                "y": 234
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "06894ce2-2d29-426c-a6c5-117a0bdbd0a0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 114,
                "offset": 0,
                "shift": 100,
                "w": 100,
                "x": 499,
                "y": 234
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "fb9d23e2-7852-4f0b-bbe0-82de1b6f11f6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 114,
                "offset": 0,
                "shift": 95,
                "w": 97,
                "x": 922,
                "y": 118
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "9bcf4a7b-dce5-43ad-a40e-f7bf886260c4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 114,
                "offset": 0,
                "shift": 92,
                "w": 91,
                "x": 829,
                "y": 118
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "ae87e385-66b0-455c-9fb7-f62179b48e15",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 114,
                "offset": 0,
                "shift": 86,
                "w": 87,
                "x": 740,
                "y": 118
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "f643a092-74d2-4854-be89-3f404bcf5f34",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 114,
                "offset": 0,
                "shift": 86,
                "w": 85,
                "x": 100,
                "y": 118
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "12191e16-f3ba-430a-96ce-e86eca49ca83",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 114,
                "offset": 0,
                "shift": 77,
                "w": 80,
                "x": 2,
                "y": 118
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "d3de223c-e5d7-499e-a68e-06fa21a8ee6b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 114,
                "offset": 0,
                "shift": 77,
                "w": 76,
                "x": 872,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "4d0cfefd-5d92-460c-ba59-549e519b86bc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 114,
                "offset": 0,
                "shift": 77,
                "w": 77,
                "x": 793,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "406b681b-8dcf-4de4-9c81-57f1807a1ff3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 114,
                "offset": 0,
                "shift": 80,
                "w": 81,
                "x": 710,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "5b1f8f1f-ab99-481d-bc87-1699768b7954",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 114,
                "offset": 0,
                "shift": 73,
                "w": 75,
                "x": 633,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "c30da6aa-31fa-4f57-916c-28ef2cebf791",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 114,
                "offset": 0,
                "shift": 95,
                "w": 97,
                "x": 534,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "65959ba9-7596-4724-8e93-9ad5e4e11e76",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 114,
                "offset": 0,
                "shift": 74,
                "w": 84,
                "x": 448,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "7548ba9b-87aa-4663-a338-11d225dac96c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 114,
                "offset": 0,
                "shift": 78,
                "w": 81,
                "x": 365,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "04138dad-030e-4ab9-978c-09e73c804108",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 114,
                "offset": 0,
                "shift": 81,
                "w": 79,
                "x": 284,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "cace3536-e825-4365-8ccc-dda56997c90e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 114,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 84,
                "y": 118
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "89096d49-c070-406e-ba91-829649895061",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 114,
                "offset": -5,
                "shift": 20,
                "w": 32,
                "x": 250,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "1760fa7d-3081-4418-92bd-22c9c6c54788",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 114,
                "offset": 1,
                "shift": 14,
                "w": 15,
                "x": 210,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "166d3611-c440-43e7-a71b-a54bee944d57",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 114,
                "offset": 1,
                "shift": 33,
                "w": 31,
                "x": 177,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "fe1c9257-9fa2-4361-99a5-8a86a483a3c3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 114,
                "offset": -1,
                "shift": 37,
                "w": 40,
                "x": 135,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "46c916ce-c476-4a50-af31-05e54ffca418",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 114,
                "offset": 8,
                "shift": 27,
                "w": 11,
                "x": 122,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "62ddf975-b07d-47ae-8125-c2d6c1d8a177",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 114,
                "offset": 1,
                "shift": 23,
                "w": 23,
                "x": 97,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "2509693d-66b4-4dba-96cf-1185e4ad5313",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 114,
                "offset": 0,
                "shift": 25,
                "w": 23,
                "x": 72,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "aa220e88-31b9-42b4-b72d-a2d3989a25d8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 114,
                "offset": 1,
                "shift": 17,
                "w": 17,
                "x": 53,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "4ae8351a-959d-460a-b71c-751d396bca12",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 114,
                "offset": 1,
                "shift": 21,
                "w": 20,
                "x": 31,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "f7a20af1-5d37-4df7-8929-9f5a364b5212",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 114,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 14,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "4dc76cce-ea0e-41ff-85e7-99261b8005a3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 114,
                "offset": 2,
                "shift": 14,
                "w": 21,
                "x": 227,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "988de603-3783-447e-8bd9-2b62f3759695",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 114,
                "offset": 1,
                "shift": 23,
                "w": 23,
                "x": 187,
                "y": 118
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "600857f4-3624-4e45-9afb-2ef371db08d6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 114,
                "offset": 0,
                "shift": 25,
                "w": 24,
                "x": 435,
                "y": 118
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "302e76d2-5742-4313-851b-5362a0fc248b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 114,
                "offset": -1,
                "shift": 13,
                "w": 15,
                "x": 212,
                "y": 118
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "1eab3a51-1a01-4006-96f8-12a8781404b9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 114,
                "offset": -1,
                "shift": 15,
                "w": 15,
                "x": 696,
                "y": 118
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "32ae6030-2e00-455e-ab18-842c1bcbbb5f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 114,
                "offset": 1,
                "shift": 26,
                "w": 26,
                "x": 668,
                "y": 118
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "ac4cf468-8728-4d2e-87e8-0ac03a741abc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 114,
                "offset": 0,
                "shift": 13,
                "w": 22,
                "x": 644,
                "y": 118
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "51cdd385-f6fa-4b61-81fa-582e8de3c595",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 114,
                "offset": -1,
                "shift": 36,
                "w": 39,
                "x": 603,
                "y": 118
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "0dc312ac-2cdc-4e56-9fc6-5bfc6e820639",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 114,
                "offset": -1,
                "shift": 25,
                "w": 27,
                "x": 574,
                "y": 118
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "bb22b609-ade5-4138-a53e-eefafba48b08",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 114,
                "offset": 1,
                "shift": 21,
                "w": 18,
                "x": 554,
                "y": 118
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "5c0400a7-8f29-4af2-8581-bae0061316ab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 114,
                "offset": -2,
                "shift": 24,
                "w": 26,
                "x": 526,
                "y": 118
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "dc4d3902-3ab0-4afb-b2aa-e0f12b8d7cae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 114,
                "offset": 1,
                "shift": 23,
                "w": 21,
                "x": 503,
                "y": 118
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "e52ca029-a365-408d-8075-82d18c800f58",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 114,
                "offset": -1,
                "shift": 19,
                "w": 22,
                "x": 479,
                "y": 118
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "ed114450-455d-44b8-bfcb-c9bd613ff99a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 114,
                "offset": -2,
                "shift": 23,
                "w": 25,
                "x": 713,
                "y": 118
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "9a1cbf99-c832-4ff7-9888-8d2e3506e5c6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 114,
                "offset": 0,
                "shift": 15,
                "w": 16,
                "x": 461,
                "y": 118
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "afff81a3-add4-4c47-8c0e-3934d59e467f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 114,
                "offset": -1,
                "shift": 25,
                "w": 27,
                "x": 406,
                "y": 118
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "0e3921b8-5bc6-4283-a7b0-5a65abc985fc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 114,
                "offset": -1,
                "shift": 23,
                "w": 25,
                "x": 379,
                "y": 118
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "f1f9b5e5-8007-4caa-be79-23311c66617b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 114,
                "offset": -1,
                "shift": 34,
                "w": 34,
                "x": 343,
                "y": 118
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "8a851e78-2247-41b8-a9ca-996d2cc8631a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 114,
                "offset": -4,
                "shift": 19,
                "w": 24,
                "x": 317,
                "y": 118
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "c9c254ba-bf8c-4443-904a-2c15396d0066",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 114,
                "offset": -1,
                "shift": 24,
                "w": 25,
                "x": 290,
                "y": 118
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "ebd552ac-f6a8-423b-bce1-5dbcf1617037",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 114,
                "offset": -1,
                "shift": 14,
                "w": 13,
                "x": 275,
                "y": 118
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "26185411-8f1a-4658-b6fe-24bd4064c7fd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 114,
                "offset": 1,
                "shift": 20,
                "w": 18,
                "x": 255,
                "y": 118
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "3f75e51a-4093-4520-abe6-5f35c955041a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 114,
                "offset": 5,
                "shift": 14,
                "w": 4,
                "x": 249,
                "y": 118
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "b3248164-e9dd-4830-bb65-02ec7d07e8fe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 114,
                "offset": 3,
                "shift": 21,
                "w": 18,
                "x": 229,
                "y": 118
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "f1c2d818-6fc0-4d7d-811e-5c8fb42bc3ee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 114,
                "offset": 0,
                "shift": 26,
                "w": 24,
                "x": 771,
                "y": 350
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "ddb81a64-cf07-41cf-b931-33f28bed69c1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 114,
                "offset": 20,
                "shift": 64,
                "w": 24,
                "x": 797,
                "y": 350
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)",
    "size": 80,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}