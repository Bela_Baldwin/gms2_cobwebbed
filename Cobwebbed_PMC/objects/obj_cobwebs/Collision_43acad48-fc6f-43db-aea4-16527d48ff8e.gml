// Counts down the remaining cobweb score upon collision

score -= 1;

// Destroys the object, as soon as it collides with the player
instance_destroy();