{
    "id": "6104f1d9-a1d2-4072-b4ba-93adf76b2878",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_player_caretaker",
    "eventList": [
        {
            "id": "f1d5c730-a077-4c86-a713-eb7dbe99bbbe",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 37,
            "eventtype": 5,
            "m_owner": "6104f1d9-a1d2-4072-b4ba-93adf76b2878"
        },
        {
            "id": "8ef08765-7b70-4c20-bba1-67ae40635745",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 39,
            "eventtype": 5,
            "m_owner": "6104f1d9-a1d2-4072-b4ba-93adf76b2878"
        },
        {
            "id": "20c751b6-8a6c-4e6a-9dc3-31a7f677a0a8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 38,
            "eventtype": 5,
            "m_owner": "6104f1d9-a1d2-4072-b4ba-93adf76b2878"
        },
        {
            "id": "faf66efd-8d76-414b-9980-c3882c234d4d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 40,
            "eventtype": 5,
            "m_owner": "6104f1d9-a1d2-4072-b4ba-93adf76b2878"
        },
        {
            "id": "7b9762e2-cdf0-4975-bd3b-dff39b407873",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "c5289a14-376f-40ce-872b-8d0c4d8bce35",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "6104f1d9-a1d2-4072-b4ba-93adf76b2878"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "4e9b5702-3a71-44f8-b48e-2536a2d70087",
    "visible": true
}