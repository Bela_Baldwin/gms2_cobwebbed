{
    "id": "469e04e7-751c-46d2-87fb-48784158d66a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_simpleStatue1",
    "eventList": [
        {
            "id": "a3e57a9a-7941-41cf-a0aa-26a04103044b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 87,
            "eventtype": 5,
            "m_owner": "469e04e7-751c-46d2-87fb-48784158d66a"
        },
        {
            "id": "9e5d515a-d031-420f-9030-5df608d960dc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 83,
            "eventtype": 5,
            "m_owner": "469e04e7-751c-46d2-87fb-48784158d66a"
        },
        {
            "id": "a801df2d-286f-444d-bc9f-8a339f38cc77",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 65,
            "eventtype": 5,
            "m_owner": "469e04e7-751c-46d2-87fb-48784158d66a"
        },
        {
            "id": "1b758935-fe7a-43fe-98f7-076079b1dfb8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 68,
            "eventtype": 5,
            "m_owner": "469e04e7-751c-46d2-87fb-48784158d66a"
        },
        {
            "id": "f5b5dfe6-a740-4b4d-93a0-35fe882496db",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 32,
            "eventtype": 9,
            "m_owner": "469e04e7-751c-46d2-87fb-48784158d66a"
        },
        {
            "id": "b5ae05b2-8fad-4dde-bedf-504effaebd39",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "469e04e7-751c-46d2-87fb-48784158d66a"
        },
        {
            "id": "a6034136-077c-4b37-8672-65874345f4ca",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "469e04e7-751c-46d2-87fb-48784158d66a"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "c5289a14-376f-40ce-872b-8d0c4d8bce35",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": true,
    "spriteId": "d7a4edad-52eb-4fe7-91d2-6dc0c203a5a0",
    "visible": true
}