// Checks if object isAlive returns true and if so
// sets it to be a solid object and isAlive to false.
// Then it randomly picks one of the other three objects of a similiar
// type and activates it.

if (isAlive = true)
{
	object_set_solid(object_index, solid);
	isAlive = false;
	
	randomJump = random_range(0,3);
	
	if (randomJump <=1)
	{
	obj_simpleStatue0.isAlive = true;
	}
	
	else if (randomJump >1 && randomJump <=2)
	{
	obj_simpleStatue2.isAlive = true;
	}
	
	else if (randomJump >2 && randomJump <=3)
	{
	obj_simpleStatue3.isAlive = true;
	}
}