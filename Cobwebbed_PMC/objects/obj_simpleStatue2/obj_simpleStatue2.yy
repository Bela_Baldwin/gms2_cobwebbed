{
    "id": "a2a15408-7515-4e3b-a49e-f54a842d3e7d",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_simpleStatue2",
    "eventList": [
        {
            "id": "f175d45d-68e2-4282-b6e1-a7a315396a21",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 87,
            "eventtype": 5,
            "m_owner": "a2a15408-7515-4e3b-a49e-f54a842d3e7d"
        },
        {
            "id": "d2433655-dc16-4b6e-9d5d-56cd162c68e3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 83,
            "eventtype": 5,
            "m_owner": "a2a15408-7515-4e3b-a49e-f54a842d3e7d"
        },
        {
            "id": "da8bf972-d3ee-4cfe-9013-8c21fd2b5853",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 65,
            "eventtype": 5,
            "m_owner": "a2a15408-7515-4e3b-a49e-f54a842d3e7d"
        },
        {
            "id": "ca4c6f24-1b3d-4842-9ffa-1452abca7f50",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 68,
            "eventtype": 5,
            "m_owner": "a2a15408-7515-4e3b-a49e-f54a842d3e7d"
        },
        {
            "id": "9337c1a8-2b4e-47f5-827b-345a44e356e2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 32,
            "eventtype": 9,
            "m_owner": "a2a15408-7515-4e3b-a49e-f54a842d3e7d"
        },
        {
            "id": "7217ce43-6eed-4a18-b106-4c84b29e8044",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "a2a15408-7515-4e3b-a49e-f54a842d3e7d"
        },
        {
            "id": "b957b550-cfcb-476f-96ca-ddcbbe28c269",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "a2a15408-7515-4e3b-a49e-f54a842d3e7d"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "c5289a14-376f-40ce-872b-8d0c4d8bce35",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": true,
    "spriteId": "d7a4edad-52eb-4fe7-91d2-6dc0c203a5a0",
    "visible": true
}