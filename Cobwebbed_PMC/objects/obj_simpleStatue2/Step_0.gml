// Checks every frame if object isAlive returns true and if so changes the objects sprite

if (isAlive = true)
{
	sprite_index = spr_livingAlter;
}
// If isAlive returns false, it changes the objects sprite back to its origin sprite
else
{
	sprite_index = spr_alter;
}