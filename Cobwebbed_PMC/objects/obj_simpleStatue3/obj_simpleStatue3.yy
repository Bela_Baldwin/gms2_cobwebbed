{
    "id": "25b75a17-c136-4501-b905-fa26682e5e01",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_simpleStatue3",
    "eventList": [
        {
            "id": "772b4873-4a44-4414-a5b8-3ea2822e39c2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 87,
            "eventtype": 5,
            "m_owner": "25b75a17-c136-4501-b905-fa26682e5e01"
        },
        {
            "id": "f79f69a4-9503-44e0-abe0-67d8a7d72363",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 83,
            "eventtype": 5,
            "m_owner": "25b75a17-c136-4501-b905-fa26682e5e01"
        },
        {
            "id": "8ca6e15e-febe-4668-bc63-8331393f1db2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 65,
            "eventtype": 5,
            "m_owner": "25b75a17-c136-4501-b905-fa26682e5e01"
        },
        {
            "id": "143d854b-d61d-4481-803a-13a3a70d0f11",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 68,
            "eventtype": 5,
            "m_owner": "25b75a17-c136-4501-b905-fa26682e5e01"
        },
        {
            "id": "1bab243e-7e98-4246-9bac-d833c79ec60d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 32,
            "eventtype": 9,
            "m_owner": "25b75a17-c136-4501-b905-fa26682e5e01"
        },
        {
            "id": "2fb905e9-526c-49b4-8efd-bfb36140d5b9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "25b75a17-c136-4501-b905-fa26682e5e01"
        },
        {
            "id": "f0ddc2c4-40f4-44f2-9ca3-88e4ac3f8af7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "25b75a17-c136-4501-b905-fa26682e5e01"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "c5289a14-376f-40ce-872b-8d0c4d8bce35",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": true,
    "spriteId": "d7a4edad-52eb-4fe7-91d2-6dc0c203a5a0",
    "visible": true
}