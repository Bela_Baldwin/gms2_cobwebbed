{
    "id": "c5289a14-376f-40ce-872b-8d0c4d8bce35",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_simpleStatues",
    "eventList": [
        {
            "id": "ce8ca549-d6e6-4630-a85a-3ea95ca6f7de",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "6104f1d9-a1d2-4072-b4ba-93adf76b2878",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "c5289a14-376f-40ce-872b-8d0c4d8bce35"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}