{
    "id": "97dbe0c4-9ebc-4c60-9ddc-022fd8b65191",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_simpleStatue0",
    "eventList": [
        {
            "id": "832b18ae-3751-4237-a68e-76b357491887",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 87,
            "eventtype": 5,
            "m_owner": "97dbe0c4-9ebc-4c60-9ddc-022fd8b65191"
        },
        {
            "id": "49d87a12-9058-4576-b6c9-a66794444c56",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 83,
            "eventtype": 5,
            "m_owner": "97dbe0c4-9ebc-4c60-9ddc-022fd8b65191"
        },
        {
            "id": "c7fc69f4-7dcc-494e-8550-7d68a1fa9e26",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 65,
            "eventtype": 5,
            "m_owner": "97dbe0c4-9ebc-4c60-9ddc-022fd8b65191"
        },
        {
            "id": "763bc643-7832-4baa-be53-2593352b0d11",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 68,
            "eventtype": 5,
            "m_owner": "97dbe0c4-9ebc-4c60-9ddc-022fd8b65191"
        },
        {
            "id": "4351d1d0-52fb-4922-9814-107e78faab11",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 32,
            "eventtype": 9,
            "m_owner": "97dbe0c4-9ebc-4c60-9ddc-022fd8b65191"
        },
        {
            "id": "48a030f2-f9f9-48d6-b447-b06405c8058e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "97dbe0c4-9ebc-4c60-9ddc-022fd8b65191"
        },
        {
            "id": "9c4848e8-7166-438b-9e38-15df222fa8af",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "97dbe0c4-9ebc-4c60-9ddc-022fd8b65191"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "c5289a14-376f-40ce-872b-8d0c4d8bce35",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": true,
    "spriteId": "d7a4edad-52eb-4fe7-91d2-6dc0c203a5a0",
    "visible": true
}