// Randomises the seed every time the game starts

randomise();

// Slightly delays randomStatueActivation in Alarm 0

ghostTrigger = 2;
alarm[0] = ghostTrigger;