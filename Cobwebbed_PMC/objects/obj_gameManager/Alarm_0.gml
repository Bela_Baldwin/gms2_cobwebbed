// Randomly sets one of the statues' isAlive as true
// and hence let's the enemy player control said statue

randomStatueActivation = random_range(0,4);

if (randomStatueActivation <=1)
{
	obj_simpleStatue0.isAlive = true;
}
	
else if (randomStatueActivation <=2)
{
	obj_simpleStatue1.isAlive = true;
}
	
else if (randomStatueActivation <=3)
{
	obj_simpleStatue2.isAlive = true;
}
	
else
{
	obj_simpleStatue3.isAlive = true;
}